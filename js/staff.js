class Staff {
    /**
     * Constructor for a Staff object. A Staff object represents a musical staff.
     * @param x : a Number representing x-coordinate of top left corner of the staff
     * @param y : a Number representing y-coordinate of the top left corner of the staff
     * @param type : a String representing the type of staff. Options: bass or treble
     * @param notes : an array of Note objects representing the notes displayed on the staff
     */
    constructor(x, y, type, notes) {
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.type = type;
        this.notes = notes;

        this.x_tracker = 150; //Keeps track of where to place a note on the x-axis based on the cumulative note lengths of the notes already placed. Starts at 150 to make room for staff image and scale identifier
        if(type == "Treble"){
            this.clefCode = "\uD834\uDD1E";
            this.note_mapper = {
                "C6": 0,
                "B5": 12.5,
                "A5": 25,
                "G5": 37.5,
                "F5": 50,
                "E5": 62.5,
                "D5": 75,
                "C5": 87.5,
                "B4": 100,
                "A4": 112.5,
                "G4": 125,
                "F4": 137.5,
                "E4": 150,
                "D4": 162.5,
                "C4": 175
            };
        } else if(type == "Bass"){
            this.clefCode = "\uD834\uDD22";
            this.note_mapper = {
                "D4": 0,
                "E4": 12.5,
                "C4": 25,
                "B3": 37.5,
                "A3": 50,
                "G3": 62.5,
                "F3": 75,
                "E3": 87.5,
                "D3": 100,
                "C3": 112.5,
                "B2": 125,
                "A2": 137.5,
                "G2": 150,
                "F2": 162.5,
                "E2": 175,
                "D2": 187.5,
                "C2": 200
            };
        }
    }

    /**
     * Display this staff and the notes on the staff
     */
    show() {
        // Display the staff
        fill(255);
        rectMode(CORNER);
        rect(this.xCoordinate, this.yCoordinate + 50, 662, 100);
        line(this.xCoordinate, this.yCoordinate + 75, this.xCoordinate + 662, this.yCoordinate + 75);
        line(this.xCoordinate, this.yCoordinate + 100, this.xCoordinate + 662, this.yCoordinate + 100);
        line(this.xCoordinate, this.yCoordinate + 125, this.xCoordinate + 662, this.yCoordinate + 125);

        fill(0);
        textAlign(LEFT, TOP);
        textSize(100);
        if(this.type == "Treble") {
            text(this.clefCode, this.xCoordinate, this.yCoordinate + 62.4);
        } else if(this.type == "Bass"){
            text(this.clefCode, this.xCoordinate, this.yCoordinate + 40.5);
        }
        // Display notes on the staff
        let xCoordinate = this.xCoordinate;
        let yCoordinate = this.yCoordinate + 12.5;
        let x_tracker = this.x_tracker;
        let note_mapper = this.note_mapper;
        this.notes.forEach(function (note) {
            note.show(xCoordinate + x_tracker, yCoordinate, note_mapper[note.name]);
            x_tracker = x_tracker + 512 * note.length;
        });
    }
}
