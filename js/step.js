/**
 * A Step object is... TODO: figure that out
 */
class Step {

    /**
     * Constructor for a Step
     * @param text : string representing the text of a lesson step
     * @param octaves : array of Octave objects
     * @param staves
     * @param expectedUserInput : array of numbers representing midi values
     * @param hint : string representing the hint for a game
     */
    constructor(text, octaves, staves, expectedUserInput = [], hint = "") {
        this.text = text;
        this.octaves = octaves;
        this.staves = staves;
        this.expectedUserInput = expectedUserInput;
        this.hint = hint;
        this.userInputIndex = 0; // keeps track of what question the user needs to answer next if this step has a game
        this.nextStepAccessible = true; // keeps track of whether all questions have been answered
        this.audioPrompt = false;

        // If this lesson does not have a game...
        if (expectedUserInput === undefined || expectedUserInput.length === 0) {
            this.isGame = false;
        }
        // If this lesson does have a game...
        else {
            this.isGame = true;
            this.nextStepAccessible = false; // makes sure user cannot proceed to next step until all questions have
                                             // been answered
        }

    }

    /**
     * Displays components of this lesson Step
     */
    show() {
        // If this lesson step is not a game...
        if (!this.isGame) {
            // Treat this step as a normal lesson step
            this.showLessonStep();
        }
        // If this lesson is a game...
        else {
            // Treat this step as a game
            this.showLessonGame();
        }
    }

    showLessonStep() {
        // Display lesson text
        document.getElementById("lessonText").innerHTML = this.text;

        // Display all octaves
        this.octaves.forEach(function (octave) {
            octave.show();
        });

        this.staves.forEach(function (staff) {
            staff.show();
        });

    }

    showLessonGame() {
        // Display Game Text
        document.getElementById("lessonText").innerHTML = "<b>Game time!</b><br>" + this.text;

        // Display all octaves
        this.octaves.forEach(function (object) {
            object.show();
        });

        this.staves.forEach(function (staff) {
            staff.show();
        });

    }

}
