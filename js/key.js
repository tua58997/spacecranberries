/**
 * Key object represents a single piano key.
 * Typically this object will contained inside an Octave object.
 */
class Key {

    /**
     * Constructor for a Key object
     * @param x : x-coordinate of top-left display position of the keyboard octave in its container
     * @param y : y-coordinate of top-left display position of the keyboard octave in its container
     * @param width : width of the key
     * @param height : height of the key
     * @param color : color of the key (typically, black or white)
     * @param note : a String representing the note the key represents. Ex: 'C#'
     * @param altNote: a String representing the alternate name for a black key. Ex: 'Db' instead of 'C#''
     * @param midi : midi number corresponding to a particular frequency of sound.
     *  Ex 1: midi 60 => C note in octave 5
     *  Ex 2: midi 61 => C# note in octave 5
     * @param keybinding : a String representing the keyboard key to bind to this piano Key object
     */
    constructor(x, y, width, height, color, note, altNote, midi, keybinding) {
        // Set member variables from parameters
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.width = width;
        this.height = height;
        this.original_color = color; // Track this so that it can return to it after being played
        this.color = color;
        this.note = note;
        this.altNote = altNote;
        this.midi = midi;
        this.keybinding = keybinding;

        // Set up default member variables
        this.showNote = 0; // Boolean to determine if note names should be shown
        this.showKeybind = 0; // Boolean to determine if keybindings should be shown

        // Create a new oscillator for this key
        this.osc = new p5.Oscillator; // Give each key object its own oscillator
        this.osc.setType("triangle"); // Use the triangle oscillator for less dissonance (compared to sine)
    }

    /**
     * Used to display this Key
     * Typically called in show() in Octave object
     */
    show() {
        // Draw the rectangle represent the key
        fill(this.color); // sets the color of the rectangle we're about to create/draw
        rectMode(CORNER); // sets the mode in which rect interprets its 4 parameters
        rect(this.xCoordinate, this.yCoordinate, this.width, this.height); // draws the rect

        // If we want to label the piano key with its musical note...
        if (this.showNote) {
            // Set label's text size and alignment
            textSize(24);
            textAlign(CENTER);

            // If key is white...
            if (this.original_color == 255) {
                // Create label with black text at the correct position
                fill(0);
                text(this.note, this.xCoordinate + this.width / 2, this.yCoordinate + this.height / 1.3);
            }
            // If key is black...
            else if (this.original_color == 0) {
                // Create label with white text at the correct position
                fill(255);
                text(this.note + "/", this.xCoordinate + this.width / 2, this.yCoordinate + this.height / 2);
                text(this.altNote, this.xCoordinate + this.width / 2, this.yCoordinate + this.height / 2 + 24);
            }
        }

        // If we want to label the piano key with its 'keybind'...
        if (this.showKeybind) {
            // Set up values for the body/border of the label
            var rectX = this.xCoordinate + this.width / 2;
            var rectY = this.yCoordinate + this.height / 1.1;
            var rectH;
            var rectW;

            // If key is white...
            if (this.original_color == 255) {
                // Set height and width of label
                rectH = this.width / 3;
                rectW = this.width / 3;
            }
            // If key is black...
            else if (this.original_color == 0) {
                // Set height and width of label
                rectH = this.width / 1.8;
                rectW = this.width / 1.8;
            }

            // Create body/border of the label
            fill(211, 211, 211);
            rectMode(CENTER); // draws rectangles from the center instead of the default top left corner
            rect(rectX, rectY, rectW, rectH, 5); // last parameter adds rounding to the corners

            // Create label text
            fill(0);
            textAlign(CENTER, CENTER); // horizontal align, vertical align
            textSize(18);
            text(this.keybinding, this.xCoordinate + this.width / 2, this.yCoordinate + this.height / 1.1);
        }
    }

    /**
     * Plays the sound of the musical note represented by this Key
     */
    play() {
        // Start the oscillator
        this.osc.freq(midiToFreq(this.midi));
        this.osc.start();
        // Change the color of the key
        this.setColor(color(149, 7, 20));
    }

    /**
     * Stops the sound of the musical note represented by this Key
     */
    stop() {
        // Stop the oscillator
        this.osc.stop();
        // Return the color of the key to its original color
        this.setColor(this.original_color);
    }

    /**
     * Sets the color field of this Key
     * @param color
     */
    setColor(color) {
        this.color = color;
    }

}
