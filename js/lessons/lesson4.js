function lesson4() {

// Declare variables
    let octaves = [];
    let steps = [];
    let octave;
    let step;

//Create Step 1, showing note names
    octave = new Octave(25, 25, 5);
    octave.toggleNoteNames();
    octave.toggleKeybinds();
    octaves.push(octave);
    octave2 = new Octave(550, 25, 6);
    octave2.toggleNoteNames();
    octaves.push(octave2);
    step = new Step("Now that we know all of the notes and scales, we can create <b>chords</b>. A chord is a combination of three or more notes to create one sound. Chords usually form the harmony of songs, often played with the left hand on a piano, while the right hand plays the melody. <br>There are four primary types of chords: major, minor, augmented, and diminished. You're going to focus on <i>triads</i>, chords formed by three notes.", octaves, []);
    steps.push(step);

//Display C major chord (C E G)
    octaves = []; //reset octaves array
    octaveMajorC = new Octave(25, 25, 5);
    octaveMajorC.key_c.setColor(155); //update colors of C E G
    octaveMajorC.key_e.setColor(155);
    octaveMajorC.key_g.setColor(155);
    octaveMajorC.toggleNoteNames();
    octaveMajorC.toggleKeybinds();
    octaves.push(octaveMajorC); //put piano with updated colors into octaves array
    octaves.push(octave2);
    step = new Step("We'll start with the major triads. Every major chord is named for its root sound- the first note of the chord. There are 12 major chords, one for each different note. This is a C major chord:", octaves, []);
    steps.push(step);

    step = new Step("C major uses the notes C, E, and G. On a real piano, you'll play the notes with your first, third, and fifth fingers. To make it easier with a mouse and keyboard, you can play it as an <i>arpeggio</i>, which means playing a chord one note at a time. C, then E, then G.", octaves, []);
    steps.push(step);

//get rid of marked C major chord
    octaves = []; //reset octaves array
    octaves.push(octave); //re-add original 2 octaves
    octaves.push(octave2);
//check for F major played (F A C)
    expectedUserInput = [65, 69, 72];
//PROBLEM: since the expected input spans 2 octaves, the user can't use the keyboard without switching octaves in the middle of the chord
//if they're holding F and A (v and n keys) then switch octaves, the F and A notes keep playing indefinitely
    step = new Step("Every major chord follows a formula. The first note, the root, is the note the chord is named for. The next note is two full steps (four semitones) up from that, and the third note is one and a half steps (three semitones) up from the second. Using this formula, you can make all of the major chords.<br>Try to play <b>F Major</b>- play the first, second, then third note of the triad.", octaves, [], expectedUserInput, "Not quite, try again! Remember the first note, the lowest sound you play, should match the name of the chord (F). The second note should be four semitones up from the previous, and the final note is three more semitones up from the previous note.");
    steps.push(step);

//STEP 6
//check for A Major played (A, C#, E)
    expectedUserInput = [69, 73, 76];
    step = new Step("Great! F Major is made of the notes F, A, and C. <br>Let's try another one: play <b>A Major</b>. Remember, the pattern is: root note, four half steps up to the next note, then three half steps up to the final.", octaves, [], expectedUserInput, "Not quite. Give it another try, using the pattern above.");
    steps.push(step);

//check for C Major
    expectedUserInput = [60, 64, 67];
    step = new Step("Once you know this pattern, you can play all twelve major triads. Try to play each one in order, starting with <b>C Major</b>.", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);

//C# Major
    expectedUserInput = [61, 65, 68];
    step = new Step("Play <b>C# Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//D Major
    expectedUserInput = [62, 66, 69];
    step = new Step("Play <b>D Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//D# Major
    expectedUserInput = [63, 67, 70];
    step = new Step("Play <b>D# Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//E Major
    expectedUserInput = [64, 68, 71];
    step = new Step("Play <b>E Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//F Major
    expectedUserInput = [65, 69, 72];
    step = new Step("Play <b>F Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//F# Major
    expectedUserInput = [66, 70, 73];
    step = new Step("Play <b>F# Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//G Major
    expectedUserInput = [67, 71, 74];
    step = new Step("Play <b>G Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//G# Major
    expectedUserInput = [68, 72, 75];
    step = new Step("Play <b>G# Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//A Major
    expectedUserInput = [69, 73, 76];
    step = new Step("Play <b>A Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//A# Major
    expectedUserInput = [70, 74, 77];
    step = new Step("Play <b>A# Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);
//B Major
    expectedUserInput = [71, 75, 78];
    step = new Step("Play <b>B Major</b>", octaves, [], expectedUserInput, "Not quite. Remember, the pattern is base note, four semitones up, then three semitones up.");
    steps.push(step);

    step = new Step("Great job! Now you know how to play all twelve major triads. Continue on to the next lesson to learn the minor triads.", octaves, []);
    steps.push(step);


    //Create Lesson 4 from all of its steps
    let lesson4 = new Lesson("Lesson 4", steps);
    return lesson4;
}