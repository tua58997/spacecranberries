function lesson2(){
    let octaves = [];
    let staves = [];
    let steps = [];
    let octave;
    let step;
    let staff;

    octave = new Octave(25, 25, 5);
    octave.toggleNoteNames();
    octaves.push(octave);
    step = new Step("Welcome to Lesson 2! In this lesson, we will take a look at how music notes are written down on a staff. Last lesson we learned how to identify the keys on a piano by their note names. Those note names are shown below as a refresher. Remember, C always comes before the grouping of two black keys!", octaves, []);
    steps.push(step);

    octaves = [];
    octave = new Octave(25, 225, 5);
    octaves.push(octave);
    staff = new Staff(25, 25, "", []);
    staves.push(staff);
    step = new Step("That group of lines above the piano is called a 'staff'. Every line and space on the staff corresponds to a different note. Which lines and spaces correspond to which notes depends on an important marker called a 'clef'.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", []);
    staves.push(staff);
    step = new Step("Clefs are always placed on the very left of a staff. This clef is known as a 'Treble clef'. It is also sometimes referred to as a 'G clef' because the swirl at the bottom of the clef circles the line where 'G' notes are placed.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [new Note("G4", 1, "")]);
    staves.push(staff);
    step = new Step("As you can see, this note falls on the same line circled by the bottom of the G clef. That means this note is a G! Notes on a staff are in alphabetical order just like on the keyboard, so to figure out the other notes you could count your way across the spaces and lines. This can take a pretty long time when you're trying to read a lot of notes. Luckily, there are some helpful tools you can use to remember where all the notes are.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", []);
    staves.push(staff);
    step = new Step("The notes on the spaces of the staff spell out the word 'Face'.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("F4", 1, "")]);
    staves.push(staff);
    step = new Step("F", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("A4", 1, "")]);
    staves.push(staff);
    step = new Step("A", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("C5", 1, "")]);
    staves.push(staff);
    step = new Step("C", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("E5", 1, "")]);
    staves.push(staff);
    step = new Step("E", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", []);
    staves.push(staff);
    step = new Step("The notes on the lines of the staff are E-G-B-D-F. There are many different sayings used to remember this sequence, but we're going to use 'Every Good Boy Deserves Fudge'.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("E4", 1, "")]);
    staves.push(staff);
    step = new Step("<b>E</b>very", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("G4", 1, "")]);
    staves.push(staff);
    step = new Step("<b>G</b>ood", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("B4", 1, "")]);
    staves.push(staff);
    step = new Step("<b>B</b>oy", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("D5", 1, "")]);
    staves.push(staff);
    step = new Step("<b>D</b>eserves", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("F5", 1, "")]);
    staves.push(staff);
    step = new Step("<b>F</b>udge", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", []);
    staves.push(staff);
    step = new Step("That covers the white keys, but what about the black keys? Well we simply put a sharp (#) or flat (b) symbol in front of the note as an indicator!", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("F4", 0.25, "Sharp"), new Note("A4", 0.25, "Flat")]);
    staves.push(staff);
    step = new Step("Here are an F sharp and an A flat! You probably noticed that these notes look a little bit different than the ones shown previously. Notes can have different appearances depending on how long you play them for. We'll cover that in a later lesson. For now, all you need to know is that the circular part of the note always indicates what note it is. Notes are also read from left to right, so if this tiny section of notes were a song you would play F# first and then Ab.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("C4", 1, "")]);
    staves.push(staff);
    step = new Step("Notes can also appear above and below the staff. For these notes you'll have to count down from E or up from F. When a note is beyond the boundaries of the staff, all the lines in between the note and staff will be shown in that spot to help you keep track of where you are. This note is on the first line beneath E, which means it's two notes before E. That's a C!", octaves, staves);
    steps.push(step);

    step = new Step("This C in particular is known as 'Middle C' because it is located in the middle of a piano. It is located on the fourth octave, which is the octave we've been using on our piano this whole time! The C on the piano below matches the C on the staff.", octaves, staves);
    steps.push(step);

    octaves = [new Octave(25, 225, 5), new Octave(550, 225, 6)];
    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("C5", 1, "")]);
    staves.push(staff);
    step = new Step("That means that this C starts the next octave!", octaves, staves);
    steps.push(step);

    step = new Step("Let's review what we learned with an exercise! Simply play the note on the piano that matches the note shown on the staff.", [], []);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("F5", 1, "")]);
    staves.push(staff);
    step = new Step("Which note is this?", octaves, staves, [77], "Try again! Remember, the lines of the treble clef staff are E-G-B-D-F. Make sure you're on the right octave!");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("A4", 1, "")]);
    staves.push(staff);
    step = new Step("How about this?", octaves, staves, [69], "Try again! Remember, the spaces of the treble clef staff spell out FACE. Make sure you're on the right octave!");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("D4", 1, "Sharp")]);
    staves.push(staff);
    step = new Step("Let's try this one.", octaves, staves, [63], "Try again! Remember, sharps and flats on the staff are just like sharps and flats on the piano. Make sure you're on the right octave!");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Treble", [new Note("C5", 1, "")]);
    staves.push(staff);
    step = new Step("One more!", octaves, staves, [72], "Try again! Remember, the spaces of the treble clef staff spell out FACE. Make sure you're on the right octave!");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", []);
    staves.push(staff);
    step = new Step("Nice job! The treble clef isn't the only marker that might appear on the staff. There is also the 'Bass' or 'F clef'. The reason it is called the 'F' clef is because the two dots surround the line that marks F. The spaces on the bass clef staff are A-C-E-G. You can remember this with the phrase 'All Cows Eat Grass'.", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("A2", 1, "")]);
    staves.push(staff);
    step = new Step("<b>A</b>ll", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("C3", 1, "")]);
    staves.push(staff);
    step = new Step("<b>C</b>ows", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("E3", 1, "")]);
    staves.push(staff);
    step = new Step("<b>E</b>at", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("G3", 1, "")]);
    staves.push(staff);
    step = new Step("<b>G</b>rass", [], staves);
    steps.push(step);

    step = new Step("The lines of the bass clef staff are 'G-B-D-F-A'. You can remember this with the phrase 'Good Boys Don't Fail Algebra'.", [], []);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("G2", 1, "")]);
    staves.push(staff);
    step = new Step("<b>G</b>ood", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("B2", 1, "")]);
    staves.push(staff);
    step = new Step("<b>B</b>oys", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("D3", 1, "")]);
    staves.push(staff);
    step = new Step("<b>D</b>on't", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("F3", 1, "")]);
    staves.push(staff);
    step = new Step("<b>F</b>ail", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("A3", 1, "")]);
    staves.push(staff);
    step = new Step("<b>A</b>lgebra", [], staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("C4", 1, "")]);
    staves.push(staff);
    step = new Step("Middle C on the bass clef is all the way up here! While the treble clef mostly coverse the 4th and 5th octaves, the bass clef mostly covers the lower 2nd and 3rd octaves. That's why it's called the bass clef! For the upcoming exercises your piano will use the 2nd and 3rd octavevs.", [], staves);
    steps.push(step);

    octaves = [new Octave(25, 225, 3), new Octave(550, 225, 4)];
    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("G2", 1, "")]);
    staves.push(staff);
    step = new Step("Which note is this?", octaves, staves, [43], "Try again! Remember, the lines of the bass clef staff are G-B-D-F-A. Make sure you're on the right octave!");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 5, "Bass", [new Note("E3", 1, "")]);
    staves.push(staff);
    step = new Step("Which note is this?", octaves, staves, [52], "Try again! Remember, the spaces of the bass clef staff are A-C-E-G. Make sure you're on the right octave!");
    steps.push(step);

    step = new Step("Excellent work! You've just completed your second CranJam music lesson! Here's a little summary of what we covered:<ul><li>The treble clef signifies that the spaces of the staff are F-A-C-E and the lines are E-G-B-D-F.</li><li>The bass clef signifies that the spaces of the staff are A-C-E-G and the lines are G-B-D-F-A.</li></ul>On the next lesson you will learn how to create scales!", [], []);
    steps.push(step);

    let lesson2 = new Lesson("Lesson 2", steps);
    return lesson2;
}
