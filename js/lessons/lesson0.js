function lesson0() {
// Declare variables
    let octaves = [];
    let staves = [];
    let steps = [];
    let octave;
    let step;

// Create Step 1
    octave = new Octave(25, 25, 5);
    octaves.push(octave);
    step = new Step("Welcome to your first CranJam music lesson!<br>You can also move to the next step by clicking the 'Next' button. Or, if you ever need to go back and read something again, clicking the 'Previous' button will bring you back a step.", [], staves);
    steps.push(step);

// Create Step 2
    step = new Step("Below is a section of the SC Deluxe Digital Piano (patent pending). You're just in time to give it a test run! Try clicking on some keys and see how they sound.", octaves, staves);
    steps.push(step);

// Create Step 3
    octaves = [];
    octave2 = new Octave(25, 25, 5);
    octave2.toggleKeybinds();
    octaves.push(octave2);
    step = new Step("You can also control the piano using your keyboard! The overlay below shows which keys on your keyboard control which keys on the piano. You can toggle this off and on at any time by pressing the letter <b>'t'</b> on your keyboard", octaves, staves);
    steps.push(step);

    step = new Step("You've completed the How To lesson! Now it's time to move to your first music lesson!<br><b>Select Lesson 1</b> from the Lessons menu above, to continue.", [], staves);
    steps.push(step);

// Create "How To" Lesson from steps
    let lesson0 = new Lesson("How To", steps);
    return lesson0;
}
