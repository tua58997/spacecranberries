function lesson3(){
    let octaves = [];
    let staves = [];
    let steps = [];
    let octave;
    let step;
    let staff;
    let middleC = new Note("C4", 0.25, "");

    octave = new Octave(25, 25, 5);
    octaves.push(octave);
    step = new Step("Welcome to Lesson 3! In this lesson, we will learn about the different intervals between notes and how these are formed into scales.", octaves, []);
    steps.push(step);

    step = new Step("Remember semitones? A semitone is the same as a half step, or a one note increment on the piano (including sharps and flats!). Each note has 12 different intervals since there are 12 notes in an octave. Each of these intervals has a name. Let's take a look at all these intervals using 'C' as our starting or 'root' note.", octaves, []);
    steps.push(step);

    octaves = [];
    octave = new Octave(25, 225, 5);
    octaves.push(octave);
    staff = new Staff(25, 25, "Treble", [middleC, new Note("D4", 0.25, "Flat")]);
    staves.push(staff);
    step = new Step("A one semitone interval is called a 'Minor 2nd'. C's minor 2nd is D flat.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("D4", 0.25, "")]);
    staves.push(staff);
    step = new Step("A two semitone interval is called a 'Major 2nd'. C's major 2nd is D.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("E4", 0.25, "Flat")]);
    staves.push(staff);
    step = new Step("A three semitone interval is called a 'Minor 3rd'. C's minor 3rd is E flat.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("E4", 0.25, "")]);
    staves.push(staff);
    step = new Step("A four semitone interval is called a 'Major 3rd'. C's major 3rd is E.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("F4", 0.25, "")]);
    staves.push(staff);
    step = new Step("A five semitone interval is called a 'Perfect 4th'. C's perfect 4th is F.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("F4", 0.25, "Sharp"), new Note("G4", 0.25, "Flat")]);
    staves.push(staff);
    step = new Step("A six semitone interval can be called either an 'Augmented 4th' or a 'Diminished 5th'. C's augmented 4th/diminished 5th is F sharp/G flat. (HINT: in a general sense in music, 'augmented' means raised by one semitones and 'diminished' means lowered by one semitone.)", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("G4", 0.25, "")]);
    staves.push(staff);
    step = new Step("A seven semitone interval is called a 'Perfect 5th'. C's perfect 5th is G.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("A4", 0.25, "Flat")]);
    staves.push(staff);
    step = new Step("An eight semitone interval is called a 'Minor 6th'. C's minor 6th is A flat.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("A4", 0.25, "")]);
    staves.push(staff);
    step = new Step("A nine semitone interval is called a 'Major 6th'. C's major 6th is A.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("B4", 0.25, "Flat")]);
    step = new Step("A ten semitone interval is called a 'Minor 7th'. C's minor 7th is B flat.", octaves, staves);
    steps.push(step);
    staves.push(staff);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("B4", 0.25, "")]);
    staves.push(staff);
    step = new Step("An eleven semitone interval is called a 'Major 7th'. C's major 7th is B.", octaves, staves);
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [middleC, new Note("C5", 0.25, "")]);
    staves.push(staff);
    step = new Step("Twelve semitones brings us to the next C. As you might have guessed, this interval is called an octave!", octaves, staves);
    steps.push(step);

    step = new Step("We can use combinations of these intervals to create scales. In this lesson we will cover the two most basic scales: the major scale and the minor scale.", [], []);
    steps.push(step);

    step = new Step("A major scale is made up of the root, major 2nd, major 3rd, perfect 4th, perfect 5th, major 6th, major 7th, and octave. This is a lot to remember! Luckily, there's a simple step progression you can use to work out a major scale. The progression is W-W-H-W-W-W-H where 'W' is a whole step and 'H' is a half step. Remember, a whole step is the same as two half steps!", [], []);
    steps.push(step);

    octaves = [new Octave(25, 25, 5), new Octave(550, 25, 6)];
    step = new Step("Try playing the C major scale using the W-W-H-W-W-W-H progression we talked about. Start at middle C, which is the lower C shown below.", octaves, [], [60, 62, 64, 65, 67, 69, 71, 72], "Not quite! Start at middle C and count your way up using the step progression shown.");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [new Note("C4", 0.125, ""), new Note("D4", 0.125, ""), new Note("E4", 0.125, ""), new Note("F4", 0.125, ""), new Note("G4", 0.125, ""), new Note("A4", 0.125, ""), new Note("B4", 0.125, ""), new Note("C5", 0.125, "")]);
    staves.push(staff);
    step = new Step("Great job! The C major scale is the easiest major scale to remember since it has no sharps or flats. It's just all the white keys!", [], staves);
    steps.push(step);

    step = new Step("Let's try another major scale. Play the E major scale starting at E4. (E4 refers to the E at octave 4 which is the same octave as middle C.)", octaves, [], [64, 66, 68, 69, 71, 73, 75, 76], "Not quite! Remember, the major scale progression is W-W-H-W-W-W-H.");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [new Note("E4", 0.125, ""), new Note("F4", 0.125, "Sharp"), new Note("G4", 0.125, "Sharp"), new Note("A4", 0.125, ""), new Note("B4", 0.125, ""), new Note("C5", 0.125, "Sharp"), new Note("D5", 0.125, "Sharp"), new Note("E5", 0.125, "")]);
    staves.push(staff);
    step = new Step("Great job! You might be wondering why this scale is written out the way it is. For example, why do we use F sharp instead of G flat for the second note? The rule for writing out a scale is to use every different note name once and fill in the sharps or flats as needed.", [], staves);
    steps.push(step);

    step = new Step("A minor scale is made up of the root, major 2nd, minor 3rd, perfect 4th, perfect 5th, minor 6th, minor 7th, and octave. The step progression for the minor scale is W-H-W-W-H-W-W.", [], []);
    steps.push(step);

    step = new Step("Try playing the C minor scale using the W-H-W-W-H-W-W progression we talked about.", octaves, [], [60, 62, 63, 65, 67, 68, 70, 72], "Not quite! Start at middle C and count your way up using the step progression shown.");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [new Note("C4", 0.125, ""), new Note("D4", 0.125, ""), new Note("E4", 0.125, "Flat"), new Note("F4", 0.125, ""), new Note("G4", 0.125, ""), new Note("A4", 0.125, "Flat"), new Note("B4", 0.125, "Flat"), new Note("C5", 0.125, "")]);
    staves.push(staff);
    step = new Step("Great job! Let's try one more minor scale.", [], staves);
    steps.push(step);

    step = new Step("Try playing the A minor scale.", octaves, [], [69, 71, 72, 74, 76, 77, 79, 81], "Not quite! Remember, the minor scale progression is W-H-W-W-H-W-W.");
    steps.push(step);

    staves = [];
    staff = new Staff(25, 25, "Treble", [new Note("A4", 0.125, ""), new Note("B4", 0.125, ""), new Note("C5", 0.125, ""), new Note("D5", 0.125, ""), new Note("E5", 0.125, ""), new Note("F5", 0.125, ""), new Note("G5", 0.125, ""), new Note("A5", 0.125, "")]);
    staves.push(staff);
    step = new Step("Nice work! You'll notice that the A minor scale has the same notes as the C major scale. When a major and minor scale both have the same notes they are said to be 'relative' scales.", [], staves);
    steps.push(step);

    step = new Step("Excellent work! You've just completed your third CranJam music lesson! Here's a little summary of what we covered:<ul><li>Starting at a beginning or 'root' note we can create scales by picking out certain intervals.</li><li>The major scale uses the step progression W-W-H-W-W-W-H.</li><li>The minor scale uses the step progression W-H-W-W-H-W-W.</li></ul>On the next lesson you will learn how to create chords!", [], []);
    steps.push(step);

    let lesson3 = new Lesson("Lesson 3", steps);
    return lesson3;
}