function lesson6() {

// Declare variables
    let octaves = [];
    let steps = [];
    let octave;
    let step;

//create 2 octaves
    octave = new Octave(25, 25, 5);
    octave.toggleNoteNames();
    octave.toggleKeybinds();
    octaves.push(octave);
    octave2 = new Octave(550, 25, 6);
    octave2.toggleNoteNames();
    octaves.push(octave2);

//DIMINISHED CHORDS
    step = new Step("In this lesson, we'll look at two more types of chords: diminished and augmented triads. Remember that the term diminished, in this context, means lowered by a single semitone. What is being lowered? The upper note in a minor chord.", octaves, []);
    steps.push(step);

//show minor C for comparison
    octaves = []; //reset octaves array
    octaveMinorC = new Octave(25, 25, 5);
    octaveMinorC.key_c.setColor(155); //update colors of C D# G
    octaveMinorC.key_dSharp.setColor(155);
    octaveMinorC.key_g.setColor(155);
    octaveMinorC.toggleNoteNames();
    octaveMinorC.toggleKeybinds();
    octaves.push(octaveMinorC); //put piano with updated colors into octaves array
    octaves.push(octave2);
    step = new Step("Remember from Lesson 5, this is what C Minor looks like. It's made of C, D#, and G.", octaves, []);
    steps.push(step);

//show diminished C for comparison
    octaves = []; //reset octaves array
    octaveDimC = new Octave(25, 25, 5);
    octaveDimC.key_c.setColor(155); //update colors of C D# F#
    octaveDimC.key_dSharp.setColor(155);
    octaveDimC.key_fSharp.setColor(155);
    octaveDimC.toggleNoteNames();
    octaveDimC.toggleKeybinds();
    octaves.push(octaveDimC); //put piano with updated colors into octaves array
    octaves.push(octave2);
    step = new Step("Remember from Lesson 5, this is what C Minor looks like. It's made of C, D#, and G.<br>By lowering the last note by a half-step, we get <b>C Diminished</b>, made of the notes C, D#, and G&#9837; (also known as F#).<br>All diminished triads are made of two <b>minor 3rds</b>.", octaves, []);
    steps.push(step);

    step = new Step("You can see that the pattern for a diminished chord goes base note, three semitones up, and another three semitones up.", octaves, []);
    steps.push(step);

//reset Octaves to show unmarked notes
    octaves = [];
    octaves.push(octave);
    octaves.push(octave2);
//E Dim
    expectedUserInput = [64, 67, 70];
    step = new Step("Let's practice making a few of the diminished chords. <br>Play <b>E Diminished</b>", octaves, [], expectedUserInput, "The pattern for diminished chords is base note, three semitones up, then another three semitones up.");
    steps.push(step);
//A# Dim
    expectedUserInput = [70, 73, 76];
    step = new Step("Play <b>A# Diminished</b>", octaves, [], expectedUserInput, "The pattern for diminished chords is base note, three semitones up, then another three semitones up.");
    steps.push(step);
//B Dim
    expectedUserInput = [71, 74, 77];
    step = new Step("Play <b>B Diminished</b>", octaves, [], expectedUserInput, "The pattern for diminished chords is base note, three semitones up, then another three semitones up.");
    steps.push(step);
//D Dim
    expectedUserInput = [62, 65, 68];
    step = new Step("Play <b>D Diminished</b>", octaves, [], expectedUserInput, "The pattern for diminished chords is base note, three semitones up, then another three semitones up.");
    steps.push(step);

//AUGMENTED CHORDS
    step = new Step("Now, it's time for <b>augmented chords</b>.<br>Augmented chords have the same base and middle notes as their related major chord, but the final note is a half-step higher than the major chord- it is <i>augmented</i>. This makes the pattern for augmented chords go base note, then two full steps up (four semitones), then another two full steps up. In other words, augmented chords are made of two major 3rds.", octaves, []);
    steps.push(step);
//C Aug
    expectedUserInput = [60, 64, 68];
    step = new Step("Using the pattern of two full steps between each note of an augmented chord, play <b>C Augmented</b>", octaves, [], expectedUserInput, "Not quite- make sure to follow the pattern.");
    steps.push(step);
//F Aug
    expectedUserInput = [65, 69, 73];
    step = new Step("Play <b>F Augmented</b>", octaves, [], expectedUserInput, "Remember, the pattern for augmented chords is base note, two full tones up, then two more full tones up.");
    steps.push(step);
//A# Aug
    expectedUserInput = [70, 74, 78];
    step = new Step("Play <b>A# Augmented</b>", octaves, [], expectedUserInput, "Remember, the pattern for augmented chords is base note, two full tones up, then two more full tones up.");
    steps.push(step);
//B Aug
    expectedUserInput = [71, 75, 79];
    step = new Step("Play <b>B Augmented</b>", octaves, [], expectedUserInput, "Remember, the pattern for augmented chords is base note, two full tones up, then two more full tones up.");
    steps.push(step);

//CONCLUSION
    step = new Step("You're done with lesson 5! By  now, you know about four types of <b>triad chords</b>: major, minor, diminished, and augmented chords. Every chord is named for its base note, the lowest note you play, and each type of chord follows a pattern. <br><b>Major chords</b> follow a pattern of base note, four semitones up, then three semitones up. <br><b>Minor chords</b> follow a pattern of base note, three semitones up, then four semitones up.<br><b>Diminished chords</b> follow a pattern of base note, three semitones up, then three semitones up.<br><b>Augmented chords</b> follow a pattern of base note, four semitones up, then four semitones up.", octaves, []);
    steps.push(step);

//Create Lesson 6 from all of its steps
    let lesson6 = new Lesson("Lesson 6", steps);
    return lesson6;
}