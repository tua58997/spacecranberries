function lesson1() {
// Declare variables
    let octaves = [];
    let steps = [];
    let octave;
    let step;

// Create Step 4
    octaves = [];
    octave3 = new Octave(25, 25, 5);
    octave3.toggleNoteNames();
    octaves.push(octave3);
    step = new Step("Now that you know how to control the piano, let's learn a little more about it. You've probably noticed that there are two different colors of keys: white and black. Let's worry about the white keys for now. Each white key is named after one of the first seven letters of the alphabet - A, B, C, D, E, F, G.", octaves, []);
    steps.push(step);

    step = new Step("As you can see, the keys are ordered on the piano the same way they are in the alphabet. You might have also noticed that when the piano keys reach 'G', they start over again at 'A' on the next key. So what do you think happens if we keep going?", octaves, []);
    steps.push(step);

    octaves = [];
    octave4 = new Octave(550, 25, 6);
    octave4.toggleNoteNames();
    octaves.push(octave3);
    octaves.push(octave4);
    step = new Step("The pattern repeates itself! As you move up and down the 82 keys of a full piano you'll notice the same pattern of keys repeating over and over again. Each repetition of these 7 white keys and 5 black keys is what's called an 'octave'. Each octave has the same exact notes just with different pitches. On a piano, as you move left the notes get lower, and as you move right the notes get higher. Each new octave begins at 'C'. This is for historic reasons that you don't need to worry about right now!", octaves, []);
    steps.push(step);

    step = new Step("To get a better idea of this, try playing some notes on the different octaves. Play a note on the left octave and then play the same note on the right octave. You'll notice that you are playing the same note on the right octave, just at a higher pitch. (HINT: If you are using keyboard controls, you can switch which octave you are controlling using the LEFT and RIGHT arrow keys.)", octaves, []);
    steps.push(step);

    octaves = [];
    let octave5 = new Octave(25, 25, 5);
    step = new Step("Once you recognize the pattern it's easy to pick out keys by name! Just remember that C always comes right before the group of two black keys and count your way up. After a bit of practice you'll be able to select keys by name without a second thought.", octaves, []);
    steps.push(step);

    step = new Step("Let's do a little test to get in the hang of picking out keys. I'm going to remove the note names from the keys and see if you can pick out some notes by name. Remember, if you get stuck you can always go back by clicking the 'Previous' button and refresh your memory.", octaves, []);
    steps.push(step);

    octaves = [];
    octaves.push(octave5);
    step = new Step("Let's start with an easy one! Select the 'C' key using either the mouse or keyboard.", octaves, [], [60], "Try again! Remember, 'C' always comes right before the group of two black keys.");
    steps.push(step);

    step = new Step("Great job! Try selecting the 'G' key this time.", octaves, [], [67], "Try again! If you can't remember where 'G' is, try starting from 'C' and counting your way up.");
    steps.push(step);

    step = new Step("Excellent! Let's try one more. Select the 'E' key.", octaves, [], [64], "Try again! If you can't remember where 'E' is, try starting from 'C' and counting your way up.");
    steps.push(step);

    octaves = [];
    octaves.push(octave5);
    step = new Step("Easy right? Now that you've learned how the white keys are named you're probably wondering what those '#' and 'b' symbols on the black keys mean. These are known as 'sharps'(#) and 'flats'(b). A sharp note is raised up one semitone while a flat note is raised down one semitone. So what is a semitone?", octaves, []);
    steps.push(step);

    step = new Step("A semitone, or half step, is one step up or down in the sequence of notes. This includes black keys! So, for example, moving from 'C' to 'D' would not be a half step up since there is a black key in between them. Moving a half step up from 'C' would bring you to the first black key, 'C#'. Moving a half step down from 'D' would bring you to the same key, however you would now think of it as 'Db'.", octaves, []);
    steps.push(step);

    step = new Step("Whether a black key is considered sharp or flat is dependent on the context of what other key is being modified to reach it. In some songs the first black key would be considered 'C#' while in others it would be considered 'Db' based on the key signature. We'll talk about those later! Without that context you can think of black keys as being both the sharp of the note before and the flat of the note after.", octaves, []);
    steps.push(step);

    step = new Step("Two half steps are equal to one whole step. So moving from 'C' to 'D' would be a whole step since you are moving up two notes. 'G' to 'A' would similarly be a whole step. Moving from 'G#' to 'F#' would also be a whole step.", octaves, []);
    steps.push(step);

    step = new Step("Let's try another note selection exercise with sharps and flats this time!", octaves, []);
    steps.push(step);

    octaves = [];
    octaves.push(octave5);
    step = new Step("Select 'F#'.", octaves, [], [66], "Try again! Remember, '#' means to move up one half step.");
    steps.push(step);

    step = new Step("Good job! Now try selecting 'Eb'.", octaves, [], [63], "Try again! Remember, 'b' means to move down one half step.");
    steps.push(step);

    step = new Step("Now for a trickier one. Select 'E#'.", octaves, [], [65], "Not quite! Remember the rules behind sharp notes.");
    steps.push(step);

    step = new Step("That's right! Even though 'F' is not a black key, it can still be considered 'E#' since it is one half step up from 'E'. This same thing happens between 'C' and 'B' since there is no black key between those notes either.", octaves, []);
    steps.push(step);

    step = new Step("Let's try one more. Try selecting 'Cb'.", octaves, [], [71], "Try again! Think about what note comes before 'C'.");
    steps.push(step);

    step = new Step("Awesome! You'll be a note expert in no time. Let's try a few more exercises on note intervals.", octaves, []);
    steps.push(step);

    step = new Step("What note is two whole steps down from 'G'?,", octaves, [], [63], "Try again! Remember, a whole step is two semitones.");
    steps.push(step);

    step = new Step("Great! Let's do one more. What note is three half steps up from 'F'?", octaves, [], [68], "Try again!");
    steps.push(step);

    step = new Step("Excellent work! You've just completed your very first CranJam music lesson! Here's a little summmary of what we covered:<ul><li>Notes repeat in a pattern. Each instance of this pattern is called an octave.</li><li>Each step through the notes is called a semitone or half step. Two half steps make a whole step.</li><li>A sharp (#) note is raised up one semitone. A flat (b) note is raised down one semitone.</li></ul>On the next lesson you will learn how to read notes in written form on a staff!", octaves, []);
    steps.push(step);

// Create Lesson 1 from steps
    let lesson1 = new Lesson("Lesson 1", steps);
    return lesson1;
}