function earTrainingGame() {
    // Create octaves for all steps
    octaves = [];
    octave = new Octave(25, 100, 5);
    octave.toggleKeybinds();
    octave.toggleNoteNames();
    octaves.push(octave);

    // Generate steps
    steps = generateSteps("Ear training exercise: <b>Press 'P'</b> on your keyboard to <b>play</b> note", octaves, 50, [60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71]);

    // Create Ear Training Game
    let earTrainingGame = new Lesson("Ear Training Game", steps);

    return earTrainingGame;
}

function generateSteps(text, octaves, numberOfSteps, midis) {
    let steps = []; // array to return

    let max = midis.length - 1;
    let min = 0;

    for (let i = 0; i < numberOfSteps; i++) {
        // Pick a random midi
        let randMidiIndex = Math.floor(Math.random() * (max - min + 1)) + min;
        let midi = midis[randMidiIndex];
        // Generate step with random midi
        let expectedUserInput = [midi];
        let step = new Step(text, octaves, [], expectedUserInput, "Sorry, no hints!");
        step.audioPrompt = true;
        steps.push(step);
    }
    return steps;
}