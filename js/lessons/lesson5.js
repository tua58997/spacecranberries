function lesson5() {

// Declare variables
    let octaves = [];
    let steps = [];
    let octave;
    let step;

//create 2 octaves
    octave = new Octave(25, 25, 5);
    octave.toggleNoteNames();
    octave.toggleKeybinds();
    octaves.push(octave);
    octave2 = new Octave(550, 25, 6);
    octave2.toggleNoteNames();
    octaves.push(octave2);

//Step 1
    step = new Step("Now that you can play all of the major chords on a piano, you're going to learn about the minor chords.", octaves, []);
    steps.push(step);

    step = new Step("Just like the major chords, the minor chords follow a consistent pattern.  The first note is still the one that names the chord, then the second note is 3 semitones up, and the third is four semitones up. You might notice that this makes the first and third notes the same as the major chord; the only change is that the second note is moved a half-step down.", octaves, []);
    steps.push(step);

//C Minor
    expectedUserInput = [60, 63, 67];
    step = new Step("Using this pattern, try to go through all twelve minor chords in order.<br>Start with <b>C Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//C# Minor
    expectedUserInput = [61, 64, 68];
    step = new Step("Now play <b>C# Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//D Minor
    expectedUserInput = [62, 65, 69];
    step = new Step("Play <b>D Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//D# Minor
    expectedUserInput = [63, 66, 70];
    step = new Step("Play <b>D# Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//E Minor
    expectedUserInput = [64, 67, 71];
    step = new Step("Play <b>E Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//F Minor
    expectedUserInput = [65, 68, 72];
    step = new Step("Play <b>F Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//F# Minor
    expectedUserInput = [66, 69, 73];
    step = new Step("Play <b>F# Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//G Minor
    expectedUserInput = [67, 70, 74];
    step = new Step("Play <b>G Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//G# Minor
    expectedUserInput = [68, 71, 75];
    step = new Step("Play <b>G# Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//A Minor
    expectedUserInput = [69, 72, 76];
    step = new Step("Play <b>A Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//A# Minor
    expectedUserInput = [70, 73, 77];
    step = new Step("Play <b>A# Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);
//B Minor
    expectedUserInput = [71, 74, 78];
    step = new Step("Play <b>B Minor</b>.", octaves, [], expectedUserInput, "Remember, the pattern for minor chords is base note, three semitones up, then four semitones up.");
    steps.push(step);

    step = new Step("Great! Now you know how to play 24 chords: all 12 major triads and all 12 minor triads.", octaves, []);
    steps.push(step);


    //Create Lesson 5 from all of its steps
    let lesson5 = new Lesson("Lesson 5", steps);
    return lesson5;
}