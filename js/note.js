class Note {

    /**
     * Constructor for Note objects. A Note object represent a note which can be displayed on a musical staff.
     * @param name : a String representing the name of the note. Ex: "A", "A#", "B" etc.
     * @param length : a Number representing the type of note. Ex: 1/8 note = 0.125
     * @param accidental : a String indicating whether the note is natural, sharp, or flat
     */
    constructor(name, length, accidental) {
        this.name = name;
        this.length = length;
        this.accidental = accidental;
    }

    show(x, staffY, noteY){
        let y = staffY + noteY;
        let noteCode;
        let accidentalCode;
        switch(this.length) {
            case 1:
                noteCode = "\uD834\uDD5D";
                break;
            case 0.5:
                noteCode = "\uD834\uDD5E";
                break;
            case 0.25:
                noteCode = "\uD834\uDD5F";
                break;
            case 0.125:
                noteCode = "\uD834\uDD60";
                break;
        }
        textAlign(CENTER, CENTER);
        textSize(100);
        text(noteCode, x, y - 25);
        switch(this.accidental) {
            case "Sharp":
                accidentalCode = "\u266f";
                break;
            case "Flat":
                accidentalCode = "\u266d";
                break;
            case "Natural":
                accidentalCode = "\u266e";
                break;
        }
        textAlign(CENTER, CENTER);
        textSize(25);
        text(accidentalCode, x - 30, y);
        if((noteY < 50 || 150 < noteY) && noteY%25 == 0){
            line(x - 25, y, x + 25, y);
        }
    }
}
