var osc;
var octaves = []; //Stores all the currently shown octaves
var current_octave = 0; //Tracks the location of the current octave in the array (always start with the leftmost octave)
var current_lesson;
var min_octave = 0;
var max_octave; //Tracks the range of octaves
var playing_key = 0; //Key that is currently being played as a result of a mouse click
var image;

/**
 * TODO: finish explanation
 */
function preload() {
    //image = loadImage("trebleClef.png");
}

/**
 * Sets up the TODO: finish this explanation
 */
function setup() {
    // Force manual drawing
    frameRate(0); // 0 fps

    // Get current lesson name from session storage (may be null)
    let current_lesson_name = sessionStorage.getItem("current_lesson_name");
    let current_lesson_index = sessionStorage.getItem("current_lesson_index");

    // TEST
    console.log("setup() > name=" + current_lesson_name + ", index=" + current_lesson_index);

    // If we don't have a current lesson name is session storage...
    if (current_lesson_name == null) {
        // Set the current lesson to display
        current_lesson = lesson0();
        // Set session storage for current_lesson
        sessionStorage.setItem("current_lesson_name", current_lesson.name);
    }
    // If we do have a current lesson name in session storage...
    else {
        // Figure out what the current lesson should be
        if (current_lesson_name === "Lesson 1") {
            current_lesson = lesson1();
        }
        else if (current_lesson_name === "Lesson 2") {
            current_lesson = lesson2();
        }
        else if (current_lesson_name === "Lesson 3") {
            current_lesson = lesson3();
        }
        else if (current_lesson_name === "Lesson 4") {
            current_lesson = lesson4();
        }
        else if (current_lesson_name === "Lesson 5") {
            current_lesson = lesson5();
        }
        else if (current_lesson_name === "Lesson 6") {
            current_lesson = lesson6();
        }
        else if (current_lesson_name === "Ear Training Game") {
            current_lesson = earTrainingGame();
        }
        else {
            current_lesson = lesson0();
        }
    }

    if (current_lesson_index != null){
        // Set the current lesson index (what step in the lesson we are on)
        current_lesson.index = Number(current_lesson_index);
    }

    // Set up canvas
    var canvas = createCanvas(2000, 1500);
    canvas.parent("canvas"); //this canvas will go inside the html div with id="canvas"

    // Draw display
    draw();
}

/**
 * Displays everything on the canvas
 */
function draw() {
    background(255);

    //image(image, 0, 0);
    current_lesson.show();
    if(current_lesson.steps[current_lesson.index].octaves[0] != undefined){
        drawOctaveNumbers();
    }
    console.log("draw() > current_lesson=" + current_lesson);

}

function drawOctaveNumbers(){
    let octaves = current_lesson.steps[current_lesson.index].octaves;
    let selected_octave = octaves[current_octave];
    octaves.forEach(function(octave){
       if(octave == selected_octave){
            fill(0);
       } else {
            fill(150);
       }
       text("Octave " + (octave.octaveNumber - 1), octave.xCoordinate + 262.5, octave.yCoordinate + 425);
    });
}

/**
 * Called when a user selects a lesson in the Lesson Selection Menu.
 * The value of lesson is hard-coded in the html of the <a>.
 * Ex: lesson = lesson1
 */
function lessonMenuSelect(lesson) {
    current_lesson = lesson;

    // Set session storage for current_lesson
    sessionStorage.setItem("current_lesson_name", current_lesson.name);
    // Set session storage for current_lesson
    sessionStorage.setItem("current_lesson_index", current_lesson.index);

    // TEST: Get current lesson name from session storage
    let current_lesson_name = sessionStorage.getItem("current_lesson_name");
    let current_lesson_index = sessionStorage.getItem("current_lesson_index");
    console.log("lessonMenuSelect() > name=" + current_lesson_name + ", index=" + current_lesson_index);

    // Clear the text in the game notification area and the game hint area and the game hint area
    resetGameNotificationArea();
    resetGameHintArea();

    // Redraw display
    draw();
}

/**
 * Called when the "Next" button is clicked.
 * Goes to the next lesson, if possible.
 * Disables Next button and activates Previous button, if necessary.
 */
function nextBtnClick() {
    // If we're not already on the last step of the current lesson AND next step is accessible...
    if (current_lesson.index < current_lesson.steps.length - 1 && current_lesson.steps[current_lesson.index].nextStepAccessible) {
        // Get the current lesson step
        let current_lessonStep = current_lesson.steps[current_lesson.index];

        // Get the current octave for this lesson step
        let octave = current_lessonStep.octaves[current_octave];
        if(octave != undefined) {
            octave.stopAll(); //Stop all keys from playing
        }

        current_lesson.index++; // go to the next lesson step

        // Set session storage for current_lesson_index
        sessionStorage.setItem("current_lesson_index", current_lesson.index);

        // TEST: Get current lesson name from session storage
        let current_lesson_name = sessionStorage.getItem("current_lesson_name");
        let current_lesson_index = sessionStorage.getItem("current_lesson_index");
        console.log("lessonMenuSelect() > name=" + current_lesson_name + ", index=" + current_lesson_index);

        // Clear the text in the game notification area and the game hint area
        resetGameNotificationArea();
        resetGameHintArea();

        // Reset userInputIndex every time the lesson step changes
        resetUserInputIndex();

        // Activate Previous button
        document.getElementById("prevBtn").className = "btn btn-primary navbar-btn activate";
    }

    // If we're (now) on the last step of the current lesson OR next step is not accessible...
    if (current_lesson.index === current_lesson.steps.length - 1 || !current_lesson.steps[current_lesson.index].nextStepAccessible) {
        // Disable Next button
        document.getElementById("nextBtn").className = "btn btn-primary navbar-btn disabled";
    }

    // Redraw display
    draw();
}

/**
 * Called when the "Previous" button is clicked.
 * Goes to the previous lesson, if possible.
 * Disables Previous button and activates Next button, if necessary.
 */
function prevBtnClick() {
    // Clear the text in the game notification area and the game hint area
    resetGameNotificationArea();
    resetGameHintArea();

    // If we're not on the first step of the current lesson...
    if (current_lesson.index > 0) {
        // Get the current lesson step
        let current_lessonStep = current_lesson.steps[current_lesson.index];

        // Get the current octave for this lesson step
        let octave = current_lessonStep.octaves[current_octave];
        if(octave != undefined) {
            octave.stopAll(); // Stop all keys from playing
        }

        current_lesson.index--; // go to the previous lesson step

        // Set session storage for current_lesson_index
        sessionStorage.setItem("current_lesson_index", current_lesson.index);

        // TEST: Get current lesson name from session storage
        let current_lesson_name = sessionStorage.getItem("current_lesson_name");
        let current_lesson_index = sessionStorage.getItem("current_lesson_index");
        console.log("lessonMenuSelect() > name=" + current_lesson_name + ", index=" + current_lesson_index);

        // Reset userInputIndex every time the lesson step changes
        resetUserInputIndex();

        // Activate next button
        document.getElementById("nextBtn").className = "btn btn-primary navbar-btn activate";
    }

    // If we're (now) on the first step of the current lesson...
    if (current_lesson.index === 0) {
        // Disable Previous button
        document.getElementById("prevBtn").className = "btn btn-primary navbar-btn disabled";
    }

    // Redraw display
    draw();
}

/**
 * Figure out what the current lesson step is and reset the user input index.
 * Typically called whenever the current_lesson.index is changed
 */
function resetUserInputIndex() {
    current_lessonStep = current_lesson.steps[current_lesson.index];
    current_lessonStep.userInputIndex = 0;
}

/**
 * Resets the game notification area text.
 * Called by the prevBtnClick() and nextBtnClick() functions
 */
function resetGameNotificationArea() {
    document.getElementById("gameNotificationArea").style = "visibility: hidden;";
    document.getElementById("gameNotificationArea").innerHTML = "&nbsp";
}

/**
 * Resets the game hint area text.
 * Called by the prevBtnClick() and nextBtnClick() functions
 */
function resetGameHintArea() {
    document.getElementById("gameHintArea").style = "visibility: hidden; ";
    document.getElementById("gameHintArea").innerHTML = "&nbsp";
}

/**
 * TODO: description
 *
 This just checks the pixel where the mouse is on press and plays the appropriate note based on what key that is.
 Cleaned up this section a bit and also made it work regardless of number of octaves.
 */
function mousePressed() {
    // If current lesson step has an octave to show...
    if (current_lesson.steps[current_lesson.index].octaves[0] != undefined) {
        var leftX = current_lesson.steps[current_lesson.index].octaves[0].xCoordinate;
        var rightX = current_lesson.steps[current_lesson.index].octaves.length * 525 + leftX;
        var topY = current_lesson.steps[current_lesson.index].octaves[0].yCoordinate;
        var bottomY = topY + 375;
        if (leftX < mouseX && mouseX < rightX && topY < mouseY && mouseY < bottomY) {
            var keyNumber = floor((mouseX - leftX) / 75) % 7; //This gives the key number on the octave from 0-6
            var octaveNumber = floor((mouseX - leftX) / 525); //This gives the octave number
            var keySection = (mouseX - leftX) % 75; //This gives the pixel within the key from 0-99
            switch (keyNumber) {
                case 0:
                case 3: //C and F have the same structure with one black key to the right
                    if (mouseY < topY + 225) { //Vertical part of the key where black is showing
                        if (keySection < 52.5) { //Horizontal part of the key where white is
                            whiteKeyClick(keyNumber, octaveNumber);
                        } else { //Horizontal part of the key where black is
                            blackKeyClick(keyNumber, octaveNumber);
                        }
                    } else { //Vertical part of the key beneath where black is showing, will always play white key.
                        whiteKeyClick(keyNumber, octaveNumber);
                    }
                    break;
                case 1:
                case 4:
                case 5: //D, G, and A have the same structure with black keys on both sides
                    if (mouseY < topY + 225) { //Vertical part of the key where black is showing
                        if (keySection < 22.5) { //Horizontal part of the key with first black section
                            blackKeyClick(keyNumber - 1, octaveNumber); //Subtract 1 from the key number since it's the previous black key
                        } else if (keySection > 52.5) { //Horizontal part of the key with second black section
                            blackKeyClick(keyNumber, octaveNumber);
                        } else { //Horizontal part of the key with just white
                            whiteKeyClick(keyNumber, octaveNumber);
                        }
                    } else { //Vertical part of the key beneath where black is showing, will always play white key.
                        whiteKeyClick(keyNumber, octaveNumber);
                    }
                    break;
                case 2:
                case 6: //E and B have the same structure with one black key to the left
                    if (mouseY < topY + 225) { //Vertical part of the key where black is showing
                        if (keySection > 22.5) { //Horizontal part of the key where white is
                            whiteKeyClick(keyNumber, octaveNumber);
                        } else { //Horizontal part of the key where black is
                            blackKeyClick(keyNumber - 1, octaveNumber); //Subtract 1 from the key number since it's the previous black key
                        }
                    } else { //Vertical part of the key beneath where black is showing, will always play white key.
                        whiteKeyClick(keyNumber, octaveNumber);
                    }
                    break;
            }
        }

        // Redraw display
        draw();
    }
}

/**
 * Stops any music currently playing as a result of a mousePressed() call
 */
function mouseReleased() {
    if (playing_key) {
        playing_key.stop();
        playing_key = 0;
    }

    // Redraw display
    draw();
}

/**
 * TODO: Description
 */
function whiteKeyClick(keyNumber, octaveNumber) {
    // Set which key the user is playing
    playing_key = current_lesson.steps[current_lesson.index].octaves[octaveNumber].white_keys[keyNumber];

    // Play the key
    playing_key.play();

    // Evaluate the user key (in case user is playing a game)
    evaluateUserInput(playing_key.midi);
}

/**
 * TODO: Description
 */
function blackKeyClick(keyNumber, octaveNumber) {
    if (keyNumber > 2) { // Since white keys outnumber black keys, after the 'E-F' section on the piano the white key's location in the array will always be one greater than the corresponding black key
        // Set which key the user is playing
        playing_key = current_lesson.steps[current_lesson.index].octaves[octaveNumber].black_keys[keyNumber - 1];
    } else {
        // Set which key the user is playing
        playing_key = current_lesson.steps[current_lesson.index].octaves[octaveNumber].black_keys[keyNumber];
    }

    // Play the key
    playing_key.play();

    // Evaluate the user key (in case user is playing a game)
    evaluateUserInput(playing_key.midi);
}

/**
 * TODO: Description
 * These are mapped in this way based on shape of piano.
 */
function keyPressed() {
    // Get the current lesson step
    let current_lessonStep = current_lesson.steps[current_lesson.index];

    // Get the current octave for this lesson step
    let octave = current_lessonStep.octaves[current_octave];

    // Play the octave key corresponding to the keyboard key user pressed
    switch (key) {
        case 'z':
            octave.key_c.play();
            evaluateUserInput(octave.key_c.midi);
            break;
        case 's':
            octave.key_cSharp.play();
            evaluateUserInput(octave.key_cSharp.midi);
            break;
        case 'x':
            octave.key_d.play();
            evaluateUserInput(octave.key_d.midi);
            break;
        case 'd':
            octave.key_dSharp.play();
            evaluateUserInput(octave.key_dSharp.midi);
            break;
        case 'c':
            octave.key_e.play();
            evaluateUserInput(octave.key_e.midi);
            break;
        case 'v':
            octave.key_f.play();
            evaluateUserInput(octave.key_f.midi);
            break;
        case 'g':
            octave.key_fSharp.play();
            evaluateUserInput(octave.key_fSharp.midi);
            break;
        case 'b':
            octave.key_g.play();
            evaluateUserInput(octave.key_g.midi);
            break;
        case 'h':
            octave.key_gSharp.play();
            evaluateUserInput(octave.key_gSharp.midi);
            break;
        case 'n':
            octave.key_a.play();
            evaluateUserInput(octave.key_a.midi);
            break;
        case 'j':
            octave.key_aSharp.play();
            evaluateUserInput(octave.key_aSharp.midi);
            break;
        case 'm':
            octave.key_b.play();
            evaluateUserInput(octave.key_b.midi);
            break;
        case 't':
            octave.toggleKeybinds();
            draw();
            break;
        case 'p':
            promptUserWithAudio();
            break;
    }

    max_octave = current_lessonStep.octaves.length - 1;
    switch (keyCode) {
        // case ENTER:
        //    nextBtnClick();
        case RIGHT_ARROW:
            if (current_octave !== max_octave) { //Cant go further right than the max
                if(octave != undefined) {
                    octave.stopAll();
                }
                current_octave++;
            }
            break;
        case LEFT_ARROW:
            if (current_octave !== min_octave) { //Cant go further left than the min
                if(octave != undefined) {
                    octave.stopAll();
                }

                current_octave--;
            }
            break;
    }

    // Update the display
    draw();

    // // If current lesson step is a game...
    // if (current_lessonStep.isGame) {
    //     evaluateUserInput(current_lessonStep);
    // }
}

/**
 * Prompt user with audio
 */
function promptUserWithAudio() {
    // Get the current lesson step
    let current_lessonStep = current_lesson.steps[current_lesson.index];

    // If this lesson step contains an audio prompt
    if (current_lessonStep.audioPrompt) {
        //Look at expected user input to figure out what sounds to play
        let audioPrompts = current_lessonStep.expectedUserInput;
        for (let i = 0; i < audioPrompts.length; i++) {

            // Create new oscillator
            let osc = new p5.Oscillator; // Give each key object its own oscillator
            osc.setType("triangle"); // Use the triangle oscillator for less dissonance (compared to sine)

            // Start the oscillator
            osc.freq(midiToFreq(audioPrompts[i]));
            osc.start();

            // Stop the oscillator 1 second from now
            osc.stop(1);
        }
    }
}

/**
 * TODO: Description
 */
function evaluateUserInput(midiVal) {
    let current_lessonStep = current_lesson.steps[current_lesson.index];

    // If current lesson step is a game...
    if (current_lessonStep.isGame) {
        let expectedUserInput = current_lessonStep.expectedUserInput;
        let userInputIndex = current_lessonStep.userInputIndex;

        // If they user got the right answer during the game...
        if (midiVal === expectedUserInput[userInputIndex]) {
            // If we're not already on the last step of the current lesson OR there are more questions to be answered in this game step...
            if (current_lesson.index < current_lesson.steps.length - 1 || current_lessonStep.userInputIndex < expectedUserInput.length - 1) {
                document.getElementById("gameNotificationArea").style = "visibility: visible; background-color: #0A0";
                document.getElementById("gameNotificationArea").innerHTML = "Answer " + (userInputIndex + 1) + " out of " + expectedUserInput.length + " Correct!";
            }
            // If this is the last step of the current lesson AND there are no more questions to be answered in this game step...
            else {
                document.getElementById("gameNotificationArea").style = "visibility: visible; background-color: #39F";
                document.getElementById("gameNotificationArea").innerHTML = "Answer " + (userInputIndex + 1) + " out of " + expectedUserInput.length + " Correct! <b>Lesson Complete!</b>";
            }

            // If there are more questions to be answered in this game step
            if (current_lessonStep.userInputIndex < expectedUserInput.length - 1) {
                current_lessonStep.userInputIndex++;
            }
            // If there are no more questions to be answered in this game step
            else {
                current_lessonStep.nextStepAccessible = true;
                // Activate next button
                document.getElementById("nextBtn").className = "btn btn-primary navbar-btn activate";
            }
        }
        // If the user got the wrong answer during the game...
        else {
            document.getElementById("gameHintArea").style = "visibility: visible; boder: 1px solid black; background-color: #ddd";
            document.getElementById("gameHintArea").innerHTML = "<b>Hint:</b> " + current_lessonStep.hint;

            document.getElementById("gameNotificationArea").style = "visibility: visible; background-color: #f46";
            document.getElementById("gameNotificationArea").innerHTML = "Answer " + (userInputIndex + 1) + " out of " + expectedUserInput.length + " Incorrect!";
        }
    }

}

/**
 * TODO: Description
 */
function keyReleased() {
    let octave = current_lesson.steps[current_lesson.index].octaves[current_octave];
    switch (key) {
        case 'z':
            octave.key_c.stop();
            break;
        case 's':
            octave.key_cSharp.stop();
            break;
        case 'x':
            octave.key_d.stop();
            break;
        case 'd':
            octave.key_dSharp.stop();
            break;
        case 'c':
            octave.key_e.stop();
            break;
        case 'v':
            octave.key_f.stop();
            break;
        case 'g':
            octave.key_fSharp.stop();
            break;
        case 'b':
            octave.key_g.stop();
            break;
        case 'h':
            octave.key_gSharp.stop();
            break;
        case 'n':
            octave.key_a.stop();
            break;
        case 'j':
            octave.key_aSharp.stop();
            break;
        case 'm':
            octave.key_b.stop();
            break;
    }

    // Update the display
    draw();
}
