/**
 * A Lesson object contains a set of (lesson) steps
 * TODO: more to be said about this once I understand it...
 */
class Lesson {
    /**
     * Constructor for a Lesson
     * @param steps : an array of the steps in the lesson
     */
    constructor(name, steps) {
        // Initialize Lesson fields
        this.name = name;
        this.steps = steps;
        this.index = 0; // used to keep track of current step of this lesson
    }

    /**
     * Used to... TODO: figure that out
     * Typically used by... TODO: figure that out
     */
    show() {
        // Set the Lesson # in the html page
        document.getElementById("lessonNumber").innerHTML = "<b>" + this.name + "</b>: Step <b>" + (this.index + 1) + "</b> out of <b>" + this.steps.length + "</b>";

        // Show the current step of this lesson
        this.steps[this.index].show();
    }

}
