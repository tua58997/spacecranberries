/**
 *  An Octave object (typically) contains a set of 7 white keys and 5 black keys representing the 12 keys in a single
 *  octave on a piano. The first octave (number 0) is a special case with only 3 keys.
 */
class Octave {

    /**
     * Constructor for an Octave
     * @param x : x-coordinate of top-left display position of the keyboard octave in its container
     * @param y : y-coordinate of top-left display position of the keyboard octave in its container
     * @param octaveNumber : a number 0-8 representing the octave number on a piano. octaveNumber 0 is a special case
     * with 3 keys
     */
    constructor(x, y, octaveNumber) {
        // These are unused definitions and should probably be removed
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.octaveNumber = octaveNumber;


        let wW = 75;
        let bW = 45;
        let wH = 5*wW;
        let bH = 5*bW;
        let bO = wW-bW/2; // black key offset

        // Create new Key objects for a single specified octave of a keyboard
        this.key_c      = new Key(x +   0, y, wW, wH, 255, 'C',    '', octaveNumber * 12 +  0, 'z');
        this.key_cSharp = new Key(x + bO, y, bW, bH,   0, 'C#', 'Db', octaveNumber * 12 +  1, 's');
        this.key_d      = new Key(x + wW*1, y, wW, wH, 255, 'D',    '', octaveNumber * 12 +  2, 'x');
        this.key_dSharp = new Key(x + wW*1+bO, y, bW, bH,   0, 'D#', 'Eb', octaveNumber * 12 +  3, 'd');
        this.key_e      = new Key(x + wW*2, y, wW, wH, 255, 'E',    '', octaveNumber * 12 +  4, 'c');
        this.key_f      = new Key(x + wW*3, y, wW, wH, 255, 'F',    '', octaveNumber * 12 +  5, 'v');
        this.key_fSharp = new Key(x + wW*3+bO, y, bW, bH,   0, 'F#', 'Gb', octaveNumber * 12 +  6, 'g');
        this.key_g      = new Key(x + wW*4, y, wW, wH, 255, 'G',    '', octaveNumber * 12 +  7, 'b');
        this.key_gSharp = new Key(x + wW*4+bO, y, bW, bH,   0, 'G#', 'Ab', octaveNumber * 12 +  8, 'h');
        this.key_a      = new Key(x + wW*5, y, wW, wH, 255, 'A',    '', octaveNumber * 12 +  9, 'n');
        this.key_aSharp = new Key(x + wW*5+bO, y, bW, bH,   0, 'A#', 'Bb', octaveNumber * 12 + 10, 'j');
        this.key_b      = new Key(x + wW*6, y, wW, wH, 255, 'B',    '', octaveNumber * 12 + 11, 'm');

        // Setup a list of white keys
        this.white_keys = [];
        this.white_keys.push(this.key_c);
        this.white_keys.push(this.key_d);
        this.white_keys.push(this.key_e);
        this.white_keys.push(this.key_f);
        this.white_keys.push(this.key_g);
        this.white_keys.push(this.key_a);
        this.white_keys.push(this.key_b);

        // Setup a list of black keys
        this.black_keys = [];
        this.black_keys.push(this.key_cSharp);
        this.black_keys.push(this.key_dSharp);
        this.black_keys.push(this.key_fSharp);
        this.black_keys.push(this.key_gSharp);
        this.black_keys.push(this.key_aSharp);
    }

    /**
     * Used to toggle the display of note names on keys of this octave
     */
    toggleNoteNames(){
        // Run through the list of white keys in this octave
        this.white_keys.forEach(function(key){
            key.showNote = !key.showNote; // negate/toggle showNote boolean
        });
        // Run through the list of black keys in this octave
        this.black_keys.forEach(function(key){
            key.showNote = !key.showNote; // negate/toggle showNote boolean
        });
    }

    /**
     * Used to toggle the display of key binds on keys of this octave
     */
    toggleKeybinds(){
        // Run through the list of white keys in this octave
        this.white_keys.forEach(function(key){
            key.showKeybind = !key.showKeybind; // negate/toggle showKeybind boolean
        });
        // Run through the list of black keys in this octave
        this.black_keys.forEach(function(key){
            key.showKeybind = !key.showKeybind; // negate/toggle showKeybind boolean
        });
    }

    /**
     * Used to display the keys of this octave
     */
    show() {
        // Run through the list of white keys in this octave
        this.white_keys.forEach(function(key) {
            key.show(); // call the show method of the key class for this key
        });
        // Run through the list of black keys in this octave
        this.black_keys.forEach(function(key) {
            key.show(); // call the show method of the key class for this key
        });
    }

    stopAll(){
        // Run through the list of white keys in this octave
        this.white_keys.forEach(function(key) {
            key.stop(); // call the stop method of the key class for this key
        });
        // Run through the list of black keys in this octave
        this.black_keys.forEach(function(key) {
            key.stop(); // call the stop method of the key class for this key
        });
    }

}
